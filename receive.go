package main

import (
	"bytes"
	_ "encoding/json"
	"fmt"
	_ "io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

//var TIME_SLEEP_LATENCY int = 5

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

/*
type MetaPod struct {
	Uid          string
	Type         string
	Ip           string
	Node         string
	Availability float64
	Speedup      float64
}*/

func main() {

	pathProxyServiceRegistry = "http://" + os.Getenv("CLUSTER_MONITOR_HOST") + "/registrated/services/all"
	pathProxyServiceRegistryMTu = "http://" + os.Getenv("CLUSTER_MONITOR_HOST") + "/microservices/all"
	//"http://localhost:9000/state/services"
	//ntpServer = "0.it.pool.ntp.org"
	//pathMetadata = "http://localhost:8082/service/metadata"
	pathUpdateRedis = "http://localhost:8084/redis/service/update"
	pathUpdateTuRedis = "http://localhost:8084/redis/microservices/update"
	forever := make(chan bool)
	go func() {
		log.Printf("Mi metto in ricezione")
		for {
			log.Printf("Update Latency")
			//1. Prendo la lista dei pod dal master (e vedere come )
			// o da un eventuale service registry
			podList := getPodListFromRegistry()

			//se questa condizione è vera allora sono salvati su Cassandra, altrimenti
			//non sono mai stati deployati.
			if podList != nil {
				fmt.Println(podList)
				/**
				 * se li faccio ritornare dal framework monitor, allora posso già filtrarli a monte perché
				 * so che sono attivi (ricevo da cassandra la versione più aggiornata) e magari la disponibilità
				 * la calcolo lì (faccio l'eco, se non risponde aggiorno la disponibilità direttamente lì)
				 */

				//2. Stimo la latenza rispetto ai servizi ottenuti
				d := make([]int64, len(podList))
				for i := 0; i < len(podList); i++ {
					path := "http://" + podList[i].Ip + ":8084/rtt"
					d[i] = estimateLatencyV3(path) //, ntpServer)
				}

				//------------------------------------------------------------------
				//5. componi il body della Post a questo punto solo con i servizi attivi!

				rstate := Rstate{}
				//rstate.Latencies = make([]MetaLatency, len(podList)*len(podList))
				//rstate.Services = make([]MetaService, len(podList))
				for _, podMeta := range podList {
					metaService := makeMetaService(
						podMeta.Uid,
						podMeta.Type,
						podMeta.Node,
						podMeta.Ip,
						podMeta.Availability,
						1.0) //podMeta.Speedup)
					rstate.Services = append(rstate.Services, metaService)
				}

				// invoca update su Redis delle Resources

				latency := make([][]int64, len(podList))

				for l := 0; l < len(podList); l++ {
					latency[l] = make([]int64, len(podList))
				}

				for i := 0; i < len(podList); i++ {
					for j := 0; j < len(podList); j++ {
						latency[i][j] = d[i] + d[j]
						fmt.Print(" ", latency[i][j])
						/*
							// Questa cosa credo non sia corretta perché la latenza prevede che si passi
							// comunque per l'orchestratore... !!! Quindi che la latenza è 0 per due servizi sullo stesso nodo
							// non è corretto in questo scenario!!!
							if(podList[i].Node != podList[j].Node)
								latency[i][j] = d[i] + [j]
							else{
								latency[i][j] = 0 // ????
							}*/
					}
					fmt.Print("\n")
				}
				//In base alla tabella delle latenze e ai nodi scrivi su Redis

				for i := 0; i < len(podList); i++ {
					for j := 0; j < len(podList); j++ {
						metaLatency := makeMetaLatency(
							podList[i].Uid,
							podList[j].Uid,
							latency[i][j])
						rstate.Latencies = append(rstate.Latencies, metaLatency)
					}
				}
				fmt.Println("Faccio l'update su Redis")
				//5.  Faccio l'update su Redis
				_, err := http.Post(pathUpdateRedis, "application/json; charset=UTF-8", bytes.NewBuffer(StructToJson(rstate)))
				if err != nil {
					// handle error
					panic(err)
				}
			} else {
				fmt.Println("Non ci sono servizi!")
			}
			time.Sleep(time.Second * 30) //TIME_SLEEP_LATENCY)
		}
	}()

	go func() {
		log.Printf("Mi metto a monitorare il tempo di esecuzione")
		for {
			log.Printf("Update Tu")
			microservicesList := getMicroserviceTuFromRegistry()

			if microservicesList != nil {
				fmt.Println(microservicesList)
				fmt.Println("Faccio l'update su Redis")
				//5.  Faccio l'update su Redis
				_, err := http.Post(pathUpdateTuRedis, "application/json; charset=UTF-8",
					bytes.NewBuffer(StructToJson(microservicesList)))
				if err != nil {
					// handle error
					panic(err)
				}
				log.Println("Aggiornato Tu")
			} else {
				fmt.Println("Non ci sono microservizi!")
			}
			time.Sleep(time.Second * 30)
		}

	}()
	log.Printf("Start to receive Latency info")

	<-forever
}
