package main

import (
	"encoding/json"
	_ "fmt"
	"math/rand"
	"time"
)

/**
 * Funzioni per generare stringhe in maniera casuale
 *
 */

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var numberRunes = []rune("1234567890")
var letterNumberRunes = []rune("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RandNumberRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = numberRunes[rand.Intn(len(numberRunes))]
	}
	return string(b)
}

func RandLetterNumberRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterNumberRunes[rand.Intn(len(letterNumberRunes))]
	}
	return string(b)
}

func StructToJson(v interface{}) []byte {
	b, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return b
}

func JsonToString(b []byte) string {
	s := string(b)
	return s
}

func Json2Struct(v interface{}, b []byte) {
	err := json.Unmarshal(b, v)
	if err != nil {
		panic(err)
	}
}
