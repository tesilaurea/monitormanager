package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	_ "log"
	"net/http"
)

/*
func simulation() []MetaPod {

	podList := []MetaPod{}
	m1 := MetaPod{
		Uid:  "1",
		Type: "Service1",
		Ip:   "http://localhost:8084/rtt",
		Node: "Node0",
	}
	m2 := MetaPod{
		Uid:  "2",
		Type: "Service1",
		Ip:   "http://localhost:8085/rtt",
		Node: "Node0",
	}
	m3 := MetaPod{
		Uid:  "3",
		Type: "Service1",
		Ip:   "http://localhost:8086/rtt",
		Node: "Node0",
	}
	m4 := MetaPod{
		Uid:  "4",
		Type: "Service1",
		Ip:   "http://localhost:8087/rtt",
		Node: "Node0",
	}
	m5 := MetaPod{
		Uid:  "5",
		Type: "Service1",
		Ip:   "http://localhost:8088/rtt",
		Node: "Node0",
	}
	m6 := MetaPod{
		Uid:  "6",
		Type: "Service1",
		Ip:   "http://localhost:8089/rtt",
		Node: "Node0",
	}
	m7 := MetaPod{
		Uid:  "7",
		Type: "Service1",
		Ip:   "http://localhost:8090/rtt",
		Node: "Node0",
	}
	podList = append(podList, m1)
	podList = append(podList, m2)
	podList = append(podList, m3)
	podList = append(podList, m4)
	podList = append(podList, m5)
	podList = append(podList, m6)
	podList = append(podList, m7)
	return podList
}
*/
func getPodListFromRegistry() []MetaPodAvailability {
	podList := []MetaPodAvailability{}
	resp, err := http.Get(pathProxyServiceRegistry)
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nel contattare il ClusterMonitor")
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &podList); err != nil {
		//panic(err)
		fmt.Println("Errore nel parsing del body")
		return nil
	}
	return podList
}

func getMicroserviceTuFromRegistry() []MetaMicroservice {
	microserviceTuList := []MetaMicroservice{}
	resp, err := http.Get(pathProxyServiceRegistryMTu)
	if err != nil {
		// handle error
		//panic(err)
		fmt.Println("Errore nel contattare il ClusterMonitor")
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &microserviceTuList); err != nil {
		//panic(err)
		fmt.Println("Errore nel parsing del body")
		return nil
	}
	return microserviceTuList
}
