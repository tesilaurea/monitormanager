package main

import (
	"fmt"
	"net/http"
	"time"
)

func estimateLatencyV2(path string) int64 {
	s := time.Now()

	fmt.Println(path)

	resp, err := http.Get(path)
	if err != nil {
		//panic(err)
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	fmt.Println("Raggiungibile")
	delay := int64(time.Since(s).Nanoseconds())
	fmt.Println(delay)
	//now = time.Now().Nanosecond() - now
	//fmt.Println(now * (int(time.Millisecond) / int(time.Nanosecond)))
	//fmt.Println("--------------------------")
	return delay
}

func main() {
	fmt.Println(estimateLatencyV2("http://localhost:8082"))
	fmt.Println(estimateLatencyV2("http://localhost:8084/rtt"))
}
