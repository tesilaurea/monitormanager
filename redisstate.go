package main

type Rstate struct {
	Services  MetaServices  `json:"services"`
	Latencies MetaLatencies `json:"latencies"`
}

type MetaService struct {
	Uid          string  `json:"uid"`
	Type         string  `json:"type"`
	Node         string  `json:"node"`
	Ip           string  `json:"ip"`
	Availability float64 `json:"availability"`
	Speedup      float64 `json:"speedup"`
}

func makeMetaService(uid string, t string, node string, ip string, availability float64, speedup float64) MetaService {
	return MetaService{
		Uid:          uid,
		Type:         t,
		Node:         node,
		Ip:           ip,
		Availability: availability,
		Speedup:      speedup,
	}
}

type MetaLatency struct {
	UidFrom string `json:"from"`
	UidTo   string `json:"to"`
	Latency int64  `json:"latency"`
}

func makeMetaLatency(uidfrom string, uidto string, latency int64) MetaLatency {
	return MetaLatency{
		UidFrom: uidfrom,
		UidTo:   uidto,
		Latency: latency,
	}
}

type MetaServices []MetaService

type MetaLatencies []MetaLatency
