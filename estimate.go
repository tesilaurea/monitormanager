package main

import (
	"encoding/json"
	"fmt"
	"github.com/beevik/ntp"

	"io/ioutil"

	"net/http"
	"time"
)

type Rtt struct {
	Clockhost int64 `json:"clkhost"`
}

// Potrei semplicemente contattarlo e ricevere la risposta,
// senza per nulla introdurre ntp
func estimateLatency(path string, ntpServer string) int64 {
	response, _ := ntp.Query(ntpServer)
	//now := time.Now().Nanosecond()
	fmt.Println(path)
	var start int64
	if response != nil {
		t := time.Now().Add(response.ClockOffset)
		start = t.UnixNano()
	} else {
		start = int64(time.Now().Nanosecond())
	}
	var rtt Rtt
	resp, err := http.Get(path)
	if err != nil {
		//panic(err)
		fmt.Println("Non raggiungibile!")
		return 100000000000000
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("Non raggiungibile!")
		return 100000000000000
	}
	fmt.Println("Raggiungibile")
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &rtt); err != nil {
		panic(err)
	}
	//fmt.Println(rtt.Clockhost)
	tTo := rtt.Clockhost - start
	/*if tTo < 0 {
		tTo = start
	}*/
	//fmt.Println(tTo)
	response, _ = ntp.Query(ntpServer)
	var tFrom int64
	if response != nil {
		tnow := time.Now().Add(response.ClockOffset)
		tFrom = tnow.UnixNano() - rtt.Clockhost
	} else {
		tFrom = int64(time.Now().Nanosecond()) - rtt.Clockhost
	}
	/*if tFrom < 0 {
		tFrom = tnow.UnixNano()
	}*/
	//fmt.Println(tnow)
	//fmt.Println(tnow.UnixNano())

	fmt.Println(tTo)
	fmt.Println(tFrom)
	var delay int64
	if tTo < 0 || tFrom < 0 {
		delay = int64(time.Now().Nanosecond()) - start
	} else {
		delay = tTo + tFrom
	}

	fmt.Println(delay)

	//now = time.Now().Nanosecond() - now
	//fmt.Println(now * (int(time.Millisecond) / int(time.Nanosecond)))
	//fmt.Println("--------------------------")
	return delay
}

func estimateLatencyV2(path string) int64 {
	start := int64(time.Now().Nanosecond())

	//now := time.Now().Nanosecond()
	fmt.Println(path)

	resp, err := http.Get(path)
	if err != nil {
		//panic(err)
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	fmt.Println("Raggiungibile")
	delay := int64(time.Now().Nanosecond()) - start
	fmt.Println(delay)
	//now = time.Now().Nanosecond() - now
	//fmt.Println(now * (int(time.Millisecond) / int(time.Nanosecond)))
	//fmt.Println("--------------------------")
	return delay
}

func estimateLatencyV3(path string) int64 {
	s := time.Now()

	fmt.Println(path)

	resp, err := http.Get(path)
	if err != nil {
		//panic(err)
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println("Non raggiungibile!")
		return 100000000000000000
	}
	fmt.Println("Raggiungibile")
	delay := int64(time.Since(s).Nanoseconds())
	fmt.Println(delay)
	//now = time.Now().Nanosecond() - now
	//fmt.Println(now * (int(time.Millisecond) / int(time.Nanosecond)))
	//fmt.Println("--------------------------")
	return delay
}
